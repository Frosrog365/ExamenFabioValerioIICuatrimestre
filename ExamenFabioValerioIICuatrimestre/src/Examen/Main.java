/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

import javax.swing.JOptionPane;

/**
 *
 * @author Fabio Valerio
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica l = new Logica();
        int[] arregloEnteros = new int[10];
        l.generarArreglo(arregloEnteros);
        System.out.println("La suma de los números impares es de: " + l.sumaImpares(arregloEnteros));
        System.out.println("Los números primos del arreglo son: " + l.numerosPrimos(arregloEnteros));
        System.out.println("El promedio de los elementos de la lista es de: " + l.generarPromedio(arregloEnteros));
        System.out.println("El arreglo es: " + l.imprimirArreglo(arregloEnteros));
        int numOg = Integer.parseInt(JOptionPane.showInputDialog("Digite el número que va a reemplazar"));
        int numRemp = Integer.parseInt(JOptionPane.showInputDialog("Digite su número nuevo"));
        System.out.println("El arreglo ahora es: " + l.imprimirArreglo(l.reemplazarElemento(arregloEnteros, numOg, numRemp)));
        int monto = Integer.parseInt(JOptionPane.showInputDialog("Digite el monto que va a retirar: "));
        System.out.println(l.retiro(l.arregloBilletes, monto));
        System.out.println(l.imprimirBilletes(l.arregloBilletes));
        int montoDepositar = Integer.parseInt(JOptionPane.showInputDialog("Digite el denominacion a la que va a depositar: "));
        int cantidadDeposito = Integer.parseInt(JOptionPane.showInputDialog("Digite la  cantidad de billetes que va a depositar: "));
        l.deposito(l.arregloBilletes, cantidadDeposito, montoDepositar);
        System.out.println(l.imprimirBilletes(l.arregloBilletes));
        l.escribirBilletes();
    }
}
