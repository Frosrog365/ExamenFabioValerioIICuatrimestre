/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

/**
 *
 * @author Fabio Valerio
 */
public class Logica {

    /**
     * Función que genera un número random del 1 al 100
     *
     * @return un número aleatorio del 1 al 100
     */
    private int generarRandom() {
        double random = Math.random() * 100;
        return (int) random;
    }

    /**
     * Función que genera el arreglo de números random;
     *
     * @param arreglo un arreglo creado en el main
     * @return Un arreglo ya definido con los números random.
     */
    public int[] generarArreglo(int[] arreglo) {
        for (int i = 0; i < 10; i++) {
            arreglo[i] = generarRandom();
        }
        return arreglo;
    }

    /**
     * Funcion que permite buscar los números impares de un arreglo y sumarlos
     *
     * @param arreglo El arreglo del ejercicio 1 ya definido
     * @return La suma de los números impares
     */
    public int sumaImpares(int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                suma += arreglo[i];
            }
        }
        return suma;
    }

    /**
     * Metodo para sacar los números primos de un arreglo
     *
     * @param arreglo El arreglo del ejercicio 1
     * @return Un String concatenando cada número primo
     */
    public String numerosPrimos(int[] arreglo) {
        String numerosPrimosStr = "";
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0 && arreglo[i] % 3 != 0 && arreglo[i] % 5 != 0 && arreglo[i] % 7 != 0 && arreglo[i] % 11 != 0
                    && arreglo[i] % 17 != 0 && arreglo[i] % 19 != 0 && arreglo[i] % 23 != 0 && arreglo[i] % 29 != 0 && arreglo[i] % 11 != 31
                    && (arreglo[i] / arreglo[i] == 1 && arreglo[i] / 1 == arreglo[i]) || arreglo[i] == 1 || arreglo[i] == 2 || arreglo[i] == 3
                    || arreglo[i] == 5 || arreglo[i] == 7 || arreglo[i] == 11 || arreglo[i] == 17 || arreglo[i] == 19 || arreglo[i] == 23 || arreglo[i] == 29
                    || arreglo[i] == 31) {

                numerosPrimosStr += arreglo[i] + ", ";
            }
        }
        numerosPrimosStr += "\b\b";

        return "[" + numerosPrimosStr + "]";
    }

    /**
     * Método para generar el promedio de un arreglo
     *
     * @param arreglo El arreglo del ejercicio 1
     * @return El promedio de todos los elementos del arreglo
     */
    public int generarPromedio(int[] arreglo) {
        int promedio = 0;
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        promedio = suma / arreglo.length;
        return promedio;
    }
/**
 * Reemplazar un elemento de un arreglo por otro definido por el usuario
 * @param arreglo El arreglo del ejercicio 1
 * @param numOg El numero del arreglo que se va a reemplazar
 * @param numRemp El numero nuevo que va a sustituir al primer número
 * @return El arreglo ya modificado
 */
    public int[] reemplazarElemento(int[] arreglo, int numOg, int numRemp) {
        boolean esta = false;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == numOg) {
                esta = true;
                arreglo[i] = numRemp;
            }

        }
        if (esta == true) {
            return arreglo;
        }
        System.out.println("El elemento no está en el arreglo");
        return arreglo;
    }

    /**
     * Función para imprimir el arreglo
     *
     * @param arreglo El arreglo del ejercicio 1
     * @return Un String con todos los elementos del arreglo
     */
    public String imprimirArreglo(int[] arreglo) {
        String impresion = "";
        for (int i = 0; i < arreglo.length; i++) {
            impresion += arreglo[i] + ", ";
        }
        impresion += "\b\b";
        return "[" + impresion + "]";
    }
    Billetes b = new Billetes();
    Billetes billete20 = new Billetes(20000, 10);
    Billetes billete10 = new Billetes(10000, 10);
    Billetes billete5 = new Billetes(5000, 10);
    Billetes billete2 = new Billetes(2000, 10);
    Billetes billete1 = new Billetes(1000, 10);
    Billetes[] arregloBilletes = {billete20, billete10, billete5, billete2, billete1};

    /**
     * Funcion que permite retirar los billetes la lista
     * @param arregloBilletes El arreglo con las divisas
     * @param monto La cantidad de dinero que se va a retirar
     * @return Un string con el desglose de el dinero retirado
     */
    public String retiro(Billetes arregloBilletes[], int monto) {
        String desglose = "";
        int div20 = monto / 20000;
        int div10 = (monto % 20000) / 10000;
        int div5 = ((monto % 20000) % 10000) / 5000;
        int div2 = (((monto % 20000) % 10000) % 5000) / 2000;
        int div1 = ((((monto % 20000) % 10000) % 5000) % 2000) / 1000;
        if (div20 >= 1) {
            desglose += div20 + " Billetes de 20000 colones\n";
        }
        if (div10 >= 1) {
            desglose += div10 + " Billetes de 10000 colones\n";
        }
        if (div5 >= 1) {
            desglose += div5 + " Billetes de 5000 colones\n";
        }
        if (div2 >= 1) {
            desglose += div2 + " Billetes de 2000 colones\n";
        }
        if (div1 >= 1) {
            desglose += div1 + " Billetes de 1000 colones";
        }
        arregloBilletes[0].cantidad -= div20;
        arregloBilletes[1].cantidad -= div10;
        arregloBilletes[2].cantidad -= div5;
        arregloBilletes[3].cantidad -= div2;
        arregloBilletes[4].cantidad -= div1;

        return desglose;

    }
/**
 * Metodo para aumentar la cantidad de los billetes en la lista
 * @param arregloBilletes El arreglo con las denominaciones y cantidades
 * @param cantidad La nueva cantidad a sumar
 * @param divisa La denominacion a la que se le va a sumar
 * @return El arreglo de billetes modificado
 */
    public Billetes[] deposito(Billetes[] arregloBilletes, int cantidad, int divisa) {
        for (int i = 0; i < arregloBilletes.length; i++) {
            if (divisa == arregloBilletes[i].denominacion) {
                arregloBilletes[i].cantidad += cantidad;
            }
        }
        return arregloBilletes;
    }
/**
 * Metodo para imprimir los billetes
 * @param arregloBilletes El arreglo con las denominaciones y cantidades
 * @return Un String con la cantidad y denominacion
 */
    public String imprimirBilletes(Billetes[] arregloBilletes) {
        String billetesStr = "";
        for (int i = 0; i < arregloBilletes.length; i++) {
            billetesStr += "Divisa: " + arregloBilletes[i].denominacion + " Cantidad: " + arregloBilletes[i].cantidad + "\n";
        }
        return billetesStr;
    }
    EscritorBilletes eB = new EscritorBilletes();
/**
 * Metodo para Escribir los billetes en un archivo
 */
    public void escribirBilletes() {
        eB.escribirBilletes(billete20);
        eB.escribirBilletes(billete10);
        eB.escribirBilletes(billete5);
        eB.escribirBilletes(billete2);
        eB.escribirBilletes(billete1);
    }
}
