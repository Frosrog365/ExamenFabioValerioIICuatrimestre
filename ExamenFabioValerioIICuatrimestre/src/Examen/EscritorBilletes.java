/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author estudiante
 */
public class EscritorBilletes {
/**
 * Metodo para crear y escribir en un archivo TXT
 * @param nuevo El nuevo billete que se va a escrbir
 */
    public void escribirBilletes(Billetes nuevo) {
        try {
            File billetes = new File("Billetes.txt");
            FileWriter fw;
            BufferedWriter bw;
            if (billetes.exists()) {
                fw = new FileWriter(billetes,true);
                bw = new BufferedWriter(fw);
                bw.newLine();
                bw.write(nuevo.denominacion + ", " + nuevo.cantidad);
            } else {
                fw = new FileWriter(billetes,true);
                bw = new BufferedWriter(fw);
                bw.write(nuevo.denominacion + ", " + nuevo.cantidad);
            }
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
